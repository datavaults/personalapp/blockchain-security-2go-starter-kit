// MIT License 2023 Infineon Technologies AG

const initialize = () => {
  const signTransactionButton = document.getElementById('signTransaction');
  const sigResultSpan = document.getElementById('signatureResult');

  const keyInfoButton = document.getElementById('keyInfoButton');
  const keyInfoSpan = document.getElementById('keyInfoResult');

  const cardInfoButton = document.getElementById('cardInfoButton');
  const cardInfoSpan = document.getElementById('cardInfoResult');

  const keyListButton = document.getElementById('keyListButton');
  const keyListSpan = document.getElementById('keyListResult');

  const keyGenerateButton = document.getElementById('keyGenerateButton');
  const keyGenerateSpan = document.getElementById('keyGenerateResult');

  signTransactionButton.onclick = () => {
    var http_request;
    http_request = new XMLHttpRequest();
    http_request.onload = function() {
      if (http_request.status == 200) {
        sigResultSpan.innerText = http_request.response
        return;
      }
    };
    http_request.open("POST", "http://127.0.0.1:5000/signature/generate");
    http_request.setRequestHeader("Content-Type", "application/json");
    http_request.send(JSON.stringify({ 'publicKey': "0x04a0ceb3fa3c6d6e50a43e23a853135819ff04b777e067cc808510a90c7f1188caea9a4045448d10db60cba52c9ffbf6a7db8e0f4ee9fa216bfaaf1c2283add0d6", 'data': "0xab89aeba8653137646e7d0548ebfff29cc2082f51f64532c4746781083f10096", 'cardPin': "0" }));
  }

  keyInfoButton.onclick = () => {
    var http_request;
    http_request = new XMLHttpRequest();
    http_request.onload = function() {
      if (http_request.status == 200) {
        keyInfoSpan.innerText = http_request.response
        return;
      }
    };
    http_request.open("GET", "http://127.0.0.1:5000/key/0x04a0ceb3fa3c6d6e50a43e23a853135819ff04b777e067cc808510a90c7f1188caea9a4045448d10db60cba52c9ffbf6a7db8e0f4ee9fa216bfaaf1c2283add0d6");
    http_request.setRequestHeader("Content-Type", "application/json");
    http_request.send()
  }

  cardInfoButton.onclick = () => {
    var http_request;
    http_request = new XMLHttpRequest();
    http_request.onload = function() {
      if (http_request.status == 200) {
        cardInfoSpan.innerText = http_request.response
        return;
      }
    };
    http_request.open("GET", "http://127.0.0.1:5000/card");
    http_request.send()
  }

  keyListButton.onclick = () => {
    var http_request;
    http_request = new XMLHttpRequest();
    http_request.onload = function() {
      if (http_request.status == 200) {
        keyListSpan.innerText = http_request.response
        return;
      }
    };
    http_request.open("GET", "http://127.0.0.1:5000/card/keys");
    http_request.send()
  }

  keyGenerateButton.onclick = () => {
    var http_request;
    http_request = new XMLHttpRequest();
    http_request.onload = function() {
      if (http_request.status == 200) {
        keyGenerateResult.innerText = http_request.response
        return;
      }
    };
    http_request.open("POST", "http://127.0.0.1:5000/key/generate");
    http_request.setRequestHeader("Content-Type", "application/json");
    //http_request.setRequestHeader("Content-Length", 0);
    //http_request.send()
    http_request.send(JSON.stringify({ 'seed': "asdf" }));
  }
}


window.addEventListener('DOMContentLoaded', initialize)
