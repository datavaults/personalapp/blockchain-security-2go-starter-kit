# Web Example

This is a simple web example to demonstrate the interaction with the [StarterKitBridge](https://gitlab.com/datavaults/personalapp/blockchain-security-2go-starter-kit/-/tree/master/StarterKitBridge).

It is written in plain HTML/JavaScript to avoid the need for dependencies. For this reason, *index.html* can simply be opened with a Web-Browser (tested with Firefox and Chrome).

The following functions are supported, where the *-mark signals a key-dependent functionality. For demonstration purposes it targets a hard-coded public key, which needs to be changed to a valid key available on the card.

* **Sign transaction\***:  Requests a signature for the defined data.
* **Get key info\***: Reads the key information (signature counters) for the defined key.
* **Get card info**: Reads the global information from the card.
* **Get key list**: Reads all public keys from the card.
* **Generate new key**: Generates a new **persistent** key on the card.
