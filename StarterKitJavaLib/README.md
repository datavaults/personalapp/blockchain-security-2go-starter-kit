# Starter Kit Java Library

This library includes utilities and wrapper functions to handle the APDUs for communicating with the Blockchain Security2Go Starter Kit in a Java project.
The code is extracted from the Android Wallet Example (https://github.com/Infineon/BlockchainSecurity2Go-Android). Small modifications were done to allow usage with Java SE and Smart Card readers on computers.

An example for using the library is given in [src/Main.java](https://gitlab.com/datavaults/personalapp/blockchain-security-2go-starter-kit/-/tree/master/StarterKitJavaLib/src/Main.java)


# Docker Demo
A Dockerfile and build script is provided to run the Java example library inside a Docker container.
Change the device path in *docker_demo.sh* according to your platform before running the container.