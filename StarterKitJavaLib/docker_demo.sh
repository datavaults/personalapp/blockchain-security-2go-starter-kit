#Builds the docker image based on the Dockerfile
docker build -t nfc-demo .

#Runs the docker container
#NOTE: Device path may need to be changed for your setup!
#Use "usb-devices" command and add the values from the entry for the NFC reader:
#/dev/bus/usb/<Bus>/<Dev#>

docker run --device=/dev/bus/usb/002/003 nfc-demo