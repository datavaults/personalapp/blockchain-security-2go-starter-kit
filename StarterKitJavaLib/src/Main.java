import javax.smartcardio.*;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.lang.Exception;

import co.coinfinity.infineonandroidapp.infineon.apdu.*;
import co.coinfinity.infineonandroidapp.infineon.apdu.response.*;
import static co.coinfinity.infineonandroidapp.infineon.NfcUtils.AID_INFINEON_BLOCKCHAIN2GO;

import org.web3j.crypto.CryptoUtils;
import org.web3j.crypto.ECDSASignature;
import org.web3j.crypto.Sign;


public class Main {
    private static CardChannel channel;
    private static Card card;

    private static final long cardTimeout = 20 * 1000;

    // The address which will be used for signing the transaction.
    // Important: Change this value to an address available on your card, otherwise the first key will be used.
    private static final String sourceAddress = "04A0CEB3FA3C6D6E50A43E23A853135819FF04B777E067CC808510A90C7F1188CAEA9A4045448D10DB60CBA52C9FFBF6A7DB8E0F4EE9FA216BFAAF1C2283ADD0D6";

    /**
     *  Initializes the reader and waits for a card to be connected.
     *
     * @throws CardException    Connecting to the card failed
     * @throws TimeoutException Card was not found within the configured timeout
     */
    private static void initializeTerminal() throws CardException, TimeoutException {
        // show the list of available terminals
        TerminalFactory factory = TerminalFactory.getDefault();
        CardTerminals terminals = factory.terminals();

        // Check for already present cards
        channel = createChannel(terminals);

        // Wait for card insertion
        if (channel == null)
        {
            System.out.println("Insert card");
            if (!terminals.waitForChange(cardTimeout))
            {
                throw new TimeoutException("Card insertion timeout expired.");
            }
            channel = createChannel(terminals);

            if (channel == null)
            {
                throw new CardException("Channel could not be established.");
            }
        }
        selectApp(channel);
    }

    /**
     * Loops over all available terminals and tries to establish a channel to the card.
     * @param terminals
     * @return
     * @throws CardException
     */
    private static CardChannel createChannel(CardTerminals terminals) throws CardException {
        CardChannel channel = null;
        for (CardTerminal terminal : terminals.list(CardTerminals.State.CARD_INSERTION))
        {
            // Connecting to the VSC sometimes caused issues with the driver.
            if (terminal.getName().contains("Microsoft Virtual Smart Card"))
                continue;

            // establish a connection with the card
            try {
                card = terminal.connect("*");
            } catch (CardException e) {
                continue;
            }

            System.out.println("card: " + card);
            channel = card.getBasicChannel();
        }
        return channel;
    }

    /**
     * Selects the Blockchain APP on the card to initialize the protocol.
     * @param channel
     * @throws CardException
     */
    private static void selectApp(CardChannel channel) throws CardException {
        if (channel == null)
        {
            throw new CardException("Invalid channel.");
        }

        SelectApplicationApdu selectApdu = new SelectApplicationApdu(AID_INFINEON_BLOCKCHAIN2GO);
        final byte[] commandBytes = selectApdu.toBytes();
        System.out.println("Transmit: " + Arrays.toString(commandBytes));

        ResponseAPDU r = channel.transmit(new CommandAPDU(selectApdu.toBytes()));
        final byte[] responseBytes = r.getBytes();
        System.out.println("Received: " + Arrays.toString(responseBytes));
        System.out.println("response: " + r.toString());
    }

    /**
     * Signs the raw data with the smart card
     * @param dataToSign 32 byte raw data to be signed by the card
     * @return
     * @throws CardException
     */
    private static byte[] sign(byte[] dataToSign, int keyIndex) throws CardException {
        ResponseAPDU r = null;
        final byte[] commandBytes = new GenerateSignatureApdu(keyIndex, dataToSign).toBytes();
        System.out.println("Transmit: " + Arrays.toString(commandBytes));

        r = channel.transmit(new CommandAPDU(commandBytes));
        final byte[] responseBytes = r.getBytes();
        System.out.println("Received: " + Arrays.toString(responseBytes));

        GenerateSignatureResponseApdu response = new GenerateSignatureResponseApdu(r.getBytes());

        System.out.println("Signature: " + response.getSignature() + " - " + response.getGlobalSigCounterAsInteger());
        return response.getSignature();
    }

    /**
     * Returns the index of the given key by looping over all available keys of the card
     * @param publicKey
     * @return
     * @throws CardException Key was not found on the card
     */
    private static byte getKeyIndex(String publicKey) throws CardException {
        ResponseAPDU r = null;
        GetKeyInfoResponseApdu response = null;

        for (int i = 0; i < 0xFE; ++i)
        {
            r = channel.transmit(new CommandAPDU(new GetKeyInfoApdu(i).toBytes()));
            response = new GetKeyInfoResponseApdu(r.getBytes());

            if (response.getPublicKeyInHex().equals(publicKey))
                return (byte)i;

            if (i > 0 && response.getPublicKeyInHex().startsWith("000000000"))
                break;
        }
        throw new CardException("Public key not found on card.");
    }

    private static byte[] getPublicKey(int keyIndex) throws CardException {
        ResponseAPDU r = channel.transmit(new CommandAPDU(new GetKeyInfoApdu(keyIndex).toBytes()));
        GetKeyInfoResponseApdu response = new GetKeyInfoResponseApdu(r.getBytes());

        return response.getPublicKey();
    }

    /**
     * Prints the public keys of the card
     * @throws CardException
     */
    private static void printCardInformation() throws CardException {
        ResponseAPDU r = null;
        GetKeyInfoResponseApdu response = null;

        for (int i = 0; i < 0xFE; ++i)
        {
            r = channel.transmit(new CommandAPDU(new GetKeyInfoApdu(i).toBytes()));
            response = new GetKeyInfoResponseApdu(r.getBytes());

            if (i > 0 && response.getPublicKeyInHex().startsWith("000000000"))
                break;

            System.out.println("Key #" + i + " : " + response.getPublicKeyInHex());
        }
    }

    /**
     * Generates a new key pair on the card.
     * @throws CardException
     */
    private static void generateKey() throws CardException {
        ResponseAPDU r = null;
        final byte[] commandBytes = new GenerateKeyPairApdu(0).toBytes();
        System.out.println("Transmit: " + Arrays.toString(commandBytes));

        r = channel.transmit(new CommandAPDU(commandBytes));
        final byte[] responseBytes = r.getBytes();
        System.out.println("Received: " + Arrays.toString(responseBytes));
    }

    /**
     *
     * @param signature signature in DER format
     * @param message   32 byte message
     * @param publicKeyRaw raw public key as byte array
     * @return
     */
    private static boolean verifySignature(byte[] signature, byte[] message, byte[] publicKeyRaw) {
        BigInteger publicKey = Sign.publicFromPoint(publicKeyRaw);
        ECDSASignature sig = CryptoUtils.fromDerFormat(signature);

        int recId = 0;
        for (; recId < 4; recId++) {
            BigInteger candidateKey = Sign.recoverFromSignature(recId, sig, message);

            if(candidateKey != null && candidateKey.equals(publicKey))
                return true;
        }
        return false;
    }

    private static void disconnect()
    {
        // disconnect
        try {
            card.disconnect(false);
        } catch (CardException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        final byte[] dataToSign = {36, 116, 64, -28, 28, -3, -31, -70, 105, -107, 51, 96, -81, 22, -116, 56, -38, -88, 112, 13, -106, -121, -42, 106, 69, 10, -42, -99, -101, -16, -70, 62};
        initializeTerminal();

        printCardInformation();

        //Enable to generate a new key pair on the card:
        //generateKey();

        int keyIndex = 1;
        // or for given public key
        //int keyIndex = getKeyIndex(sourceAddress);

        System.out.println("Read key information for index " + keyIndex);
        byte[] publicKey = getPublicKey(keyIndex);

        System.out.println("Generate signature");
        byte[] signature = sign(dataToSign, keyIndex);

        boolean valid = verifySignature(signature, dataToSign, publicKey);
        disconnect();

        if(!valid)
            throw new Exception("Signature verification failed!");

        System.out.println("Signature verified!");
    }
}
