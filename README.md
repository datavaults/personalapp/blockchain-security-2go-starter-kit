# Blockchain Security 2Go Starter Kit

This repository collects the required utilities for the Stater Kit integration.

* **[StarterKitAPI](https://gitlab.com/datavaults/personalapp/blockchain-security-2go-starter-kit/-/tree/master/StarterKitAPI)**: API definition as summary of the supported functions.
* **[StarterKitBridge](https://gitlab.com/datavaults/personalapp/blockchain-security-2go-starter-kit/-/tree/master/StarterKitBridge)**: Locally installed software to provide access for the DataVaults App to the Starter Kit.
* **[Web_Example](https://gitlab.com/datavaults/personalapp/blockchain-security-2go-starter-kit/-/tree/master/Web_Example)**: Example website to demonstrate the integration of the functions into a web frontend.
* **[StarterKitJavaLib](https://gitlab.com/datavaults/personalapp/blockchain-security-2go-starter-kit/-/tree/master/StarterKitJavaLib)**: Helper library and utilities for integrating the Starter Kit in a Java project.
