# SPDX-FileCopyrightText: 2023 Infineon Technologies AG
# SPDX-License-Identifier: MIT

import sys
import os
import binascii
import logging as logger
from collections import namedtuple
from enum import IntEnum
from typing import Tuple
from PySide2 import QtGui
from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QMainWindow, QInputDialog, QTextEdit, \
    QGridLayout, QVBoxLayout, QSpacerItem, QSizePolicy, QMessageBox, QLineEdit, QComboBox
from PySide2.QtCore import QFile, Slot, Qt, QThread, Signal
from PySide2.QtUiTools import QUiLoader

from TransactionAdapter import TransactionAdapter
from CardTransaction import CardTransaction, CardTransactionType
import server

class ApplicationState(IntEnum):
    STATE_INIT = 0
    STATE_IDLE = 1
    STATE_TX_RECEIVED = 2
    STATE_CARD_CONNECTED = 3

class CardOptionsWidget(QWidget):
    CardInfo = namedtuple("CardInfo", "pin_enabled card_id version")

    def __init__(self, transaction_adapter: TransactionAdapter):
        super().__init__()
        self._transaction_adapter = transaction_adapter

        self._card_options = QGridLayout(self)
        self._set_pin_button: QPushButton = QPushButton("Set PIN")
        self._card_info_button: QPushButton = QPushButton("Card information")
        self._get_keys_button: QPushButton = QPushButton("Read keys")
        self._card_options.addWidget(self._set_pin_button)
        self._card_options.addWidget(self._card_info_button)
        self._card_options.addWidget(self._get_keys_button)
        self._card_options.addItem(QSpacerItem(20, 100, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self._card_info = None
        self._card_info_message_box = QMessageBox()
        self._card_info_button.clicked.connect(self._card_info_message_box.exec)

        self._key_list_message_box = QMessageBox()
        self._get_keys_button.clicked.connect(self._display_keys)


    @property
    def card_info(self):
        return self._card_info

    @card_info.setter
    def card_info(self, parameter: Tuple[bool, bytes, str]):
        if not parameter:
            return

        self._card_info = self.CardInfo(parameter[0], binascii.hexlify(parameter[1]).decode('ascii'), parameter[2])
        self._set_pin_button.setText("Change PIN" if self._card_info.pin_enabled else "Set PIN")
        self._card_info_message_box.setText(f"PIN enabled: {self._card_info.pin_enabled}\n" +
                                            f"Card ID: {self._card_info.card_id}\n" +
                                            f"Version: {self._card_info.version}")

    def _display_keys(self):
        key_list = self._transaction_adapter.get_keys()
        message_text = ""
        for key in key_list:
            message_text += binascii.hexlify(key).decode('ascii') + '\n'

        self._key_list_message_box.setTextInteractionFlags(Qt.TextSelectableByMouse)
        self._key_list_message_box.setText(message_text)
        self._key_list_message_box.exec()


class MainWindow(QMainWindow):
    TRANSACTION_FINISHED_TEXT = "Transaction complete, remove card from NFC reader!"

    def __init__(self, widget = None):
        super(MainWindow, self).__init__()
        self._current_state = ApplicationState.STATE_INIT
        self._transaction_adapter = TransactionAdapter()
        self.load_ui()

        # Start server:
        self._server_thread = QThread()
        self._server_actions = server.actions
        self._server_close_signal = Signal()
        self._start_server()

        # Card configuration dialog:
        self._card_options = CardOptionsWidget(self._transaction_adapter)
        self._remove_card_warning = QMessageBox(QMessageBox.Warning, "Warning", "New transaction received. Remove card from NFC reader to see details.")

        self._pending_transaction: CardTransaction = None
        self.change_state(ApplicationState.STATE_IDLE)
        self._transaction_adapter.card_connected.connect(self.card_connected)

    @Slot(bool, object)
    def card_connected(self, state: bool, card_info: Tuple[bool, bytes, str] = None):
        if not state and self._remove_card_warning.isVisible():
            self._remove_card_warning.close()
        elif state and self._current_state == ApplicationState.STATE_TX_RECEIVED:
            if card_info[0]: # Card is PIN locked
                pin, ok = QInputDialog.getText(None, "Secure card", "Enter card PIN:",
                                     QLineEdit.Password)
                # TODO: Pin activation

            try:
                server.request_result = self._transaction_adapter.handle_card_transaction(self._pending_transaction)
            finally:
                self._main_textedit.setText(self.TRANSACTION_FINISHED_TEXT)
                server.main_event.set()
            return

        self.change_state(ApplicationState.STATE_CARD_CONNECTED if state else ApplicationState.STATE_IDLE, card_info)

    @Slot(object)
    def card_request(self, card_transaction: CardTransaction):
        logger.debug(f"Card request received: {card_transaction.transaction_type.value}")
        server.request_result = None
        self._pending_transaction = card_transaction

        if self._current_state == ApplicationState.STATE_CARD_CONNECTED:
            self._remove_card_warning.exec_()
        else:
            self.change_state(ApplicationState.STATE_TX_RECEIVED)

    def _start_server(self):
        self._server_actions.moveToThread(self._server_thread)
        self._server_actions.card_transaction_request.connect(self.card_request)
        self._server_thread.started.connect(self._server_actions.start_server)
        self._server_thread.start()

    def load_ui(self):
        loader = QUiLoader()
        path = os.path.join(os.path.dirname(__file__), "UI/form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        self._window = loader.load(ui_file, self)
        ui_file.close()

        self.setWindowTitle("Blockchain Security 2Go Starter Kit")
        self._refresh_button = self._window.findChild(QPushButton, 'btRefresh')
        self._refresh_button.setIcon(QtGui.QIcon(os.path.join(os.path.dirname(__file__), 'UI/icon_refresh-160.png')))
        self._refresh_button.clicked.connect(self._update_readers_combobox)

        self._header_label:QLabel = self._window.findChild(QLabel, 'labelHeader')
        self._header_label.setFixedSize(300, 60)
        pixmap = QtGui.QPixmap(os.path.join(os.path.dirname(__file__), 'UI/DataVaults_Logo.png'))

        self._header_label.setPixmap(pixmap.scaled(self._header_label.size(),
                                                   Qt.AspectRatioMode.KeepAspectRatio,
                                                   Qt.TransformationMode.SmoothTransformation))

        self._main_textedit: QTextEdit = self._window.findChild(QTextEdit, 'teMain')
        self._cancel_button: QPushButton = self._window.findChild(QPushButton, 'btCancel')
        self._cancel_button.clicked.connect(self._on_cancel_transaction)
        self._main_layout: QVBoxLayout = self._window.findChild(QVBoxLayout, 'layoutMain')
        self.statusBar().setEnabled(True)

        self._readers_combobox: QComboBox = self._window.findChild(QComboBox, 'cbReaders')
        self._update_readers_combobox()

    def _update_readers_combobox(self):
        self._readers_combobox.clear()
        self._readers_combobox.insertItems(0, self._transaction_adapter.get_readers())

    def change_state(self, next_state: ApplicationState, data=None):
        prev_state = self._current_state
        self._current_state = next_state

        if prev_state == ApplicationState.STATE_IDLE:
            if next_state == ApplicationState.STATE_CARD_CONNECTED:
                self._display_card_options(data)
            elif next_state == ApplicationState.STATE_TX_RECEIVED:
                self._display_transaction_details()
        elif prev_state == ApplicationState.STATE_CARD_CONNECTED:
            if next_state == ApplicationState.STATE_IDLE:
                if self._pending_transaction:
                    self.change_state(ApplicationState.STATE_TX_RECEIVED)
                else:
                    self._display_idle()
        elif prev_state == ApplicationState.STATE_TX_RECEIVED:
            if next_state == ApplicationState.STATE_CARD_CONNECTED:
                self._display_sign_transaction()
            elif next_state == ApplicationState.STATE_IDLE:
                self._display_idle()
            elif next_state == ApplicationState.STATE_TX_RECEIVED:
                self._display_transaction_details()
        elif prev_state == ApplicationState.STATE_INIT and next_state == ApplicationState.STATE_IDLE:
            self._display_idle()

    def _display_card_options(self, card_info):
        self._main_layout.replaceWidget(self._main_textedit, self._card_options)
        self._card_options.card_info = card_info
        self._main_textedit.hide()
        self._cancel_button.hide()
        self._card_options.show()

        self.statusBar().showMessage(f"Card connected")

    def _display_transaction_details(self):
        message_text = self._pending_transaction.transaction_type.value + "\nPlace card on NFC reader to accept it.\n\n"
        self._cancel_button.show()
        self.statusBar().showMessage("Transaction received")

        if self._pending_transaction.transaction_type == CardTransactionType.KEY_INFORMATION_REQUEST:
            message_text += f"Target key: {binascii.hexlify(self._pending_transaction.public_key).decode('ascii')}"

        elif self._pending_transaction.transaction_type == CardTransactionType.SIGNATURE_REQUEST:
            message_text += f"Wallet address:\n{binascii.hexlify(self._pending_transaction.public_key).decode('ascii')}\n\n" \
                         + f"Transaction reference value:\n{binascii.hexlify(self._pending_transaction.data).decode('ascii')}"
        elif self._pending_transaction.transaction_type == CardTransactionType.REQUEST_TIMEOUT:
            message_text = self._pending_transaction.transaction_type.value
            self._pending_transaction = None
            self._current_state = ApplicationState.STATE_IDLE
            self._cancel_button.hide()
            self.statusBar().showMessage("Transaction timeout")

        self._main_textedit.setText(message_text)

        if not self._main_textedit.isVisible():
            self._main_layout.replaceWidget(self._card_options, self._main_textedit)
            self._main_textedit.show()
            self._card_options.hide()



    def _display_idle(self):
        self._main_textedit.setText("Waiting for transaction, place card on reader for configuration.")

        if not self._main_textedit.isVisible():
            self._main_layout.replaceWidget(self._card_options, self._main_textedit)
            self._main_textedit.show()
            self._card_options.hide()
        self._cancel_button.hide()

        self.statusBar().showMessage("Ready")

    def _display_sign_transaction(self):
        self.statusBar().showMessage("Signing transaction...")

    def _on_cancel_transaction(self):
        self._pending_transaction = None
        server.request_result = None
        server.main_event.set()
        self.change_state(ApplicationState.STATE_IDLE)

    def closeEvent(self, event):
        self._on_cancel_transaction()
        server.actions.stop_server()
        self._server_thread.exit()


if __name__ == "__main__":
    app = QApplication([])
    widget = MainWindow()
    widget.show()
    exit_code = app.exec_()

    sys.exit(exit_code)
