# SPDX-FileCopyrightText: 2023 Infineon Technologies AG
# SPDX-License-Identifier: MIT

import logging
import blocksec2go
from threading import Event
from blocksec2go.comm import observer
from typing import Tuple
from smartcard.System import readers
from smartcard.Exceptions import ListReadersException

nfc_utils = None


def card_connect(self):
    print("Card connected")
    nfc_utils.card_connected()


def card_disconnect(self):
    print("Card disconnected")
    nfc_utils.card_disconnected()


class NFCUtils:

    def __init__(self):
        global nfc_utils
        self._card_connected = False
        self._reader = None
        self._card_connected_callback = None
        self._card_disconnected_callback = None
        self._card_connected_event = Event()
        nfc_utils = self
        blocksec2go.add_callback(connect=card_connect, disconnect=card_disconnect)
        self._cardmonitor, self._cardobserver = observer.start()

    def card_connected(self):
        readers_list = self.get_reader_list()
        logging.debug(f"Check connected readers: {readers_list}")

        for r in readers_list:
            if not self.get_reader(r):
                logging.warning("Problem with reader or card!")
                continue

            logging.debug("Active reader found, try to activate card...")
            card_info = self.activate_card()
            if card_info:
                logging.debug(f"Activated card on reader {r}.")
                break
            logging.debug(f"Problem with reader {r}.")
        else:
            return

        self._card_connected = True
        self._card_connected_event.set()

        if self._card_connected_callback:
            self._card_connected_callback(card_info)

    def card_disconnected(self):
        self._card_connected = False
        self._reader = None
        self._card_connected_event.clear()
        logging.info('Card removed!')

        if self._card_disconnected_callback:
            self._card_disconnected_callback()

    def stop_observer(self):
        observer.stop(self._cardmonitor, self._cardobserver)

    def get_reader(self, selected_reader_name):
        """ Identifies reader via a specified name and saves it as
        its main reader object.
        """
        if (self._reader == None):
            try:
                self._reader = blocksec2go.find_reader(selected_reader_name)
                logging.info('Found the specified reader and a card!')
                return self._reader
            except Exception as details:
                if (str(details) == 'No reader found'):
                    logging.info('No card reader found!     \r', 'dev')
                    return None
                elif (str(details) == 'No card on reader'):
                    logging.info('Found reader, but no card!\r', 'dev')
                    return None
                else:
                    logging.error(str(details), 'error')
                    return None
        else:
            return self._reader

    def get_reader_list(self):
        try:
            r = [str(reader) for reader in readers()]
        except ListReadersException:
            return []

        blacklist = filter(lambda x: "virtual smart card" in x.lower(), r)
        for blacklist_item in blacklist:
            r.remove(blacklist_item)

        return r

    def activate_card(self):
        """ Enables communication to the Blockchain Security 2Go card.

        Also ensures that all other cards are rejected.
        """
        try:
            card_info = blocksec2go.select_app(self._reader)
            logging.debug('Found / reset Blockchain Security 2Go card!')
            return card_info
        except Exception as details:
            logging.error(str(details))

        return False


    def generate_keypair(self) -> int:
        """ Generates a new keypair on the Blockchain Security 2Go card.

        Keep in mind that you can not specify the keypair slot.
        This is managed by the card itself.
        """
        try:
            key_id = blocksec2go.generate_keypair(self._reader)
            if (self.valid_key(key_id)):
                logging.info('Generated a new keypair at slot ' + str(key_id))
                return key_id
            else:
                raise RuntimeError('Generated keypair has become obsolete!')
        except Exception as details:
            logging.error(str(details))

    def get_keypair_info(self, key_id):
        """ Gets keypair information from the Blockchain Security 2Go card.

        This may either be the counters specifying how many times you can
        generate a signature or the public key on a specified keypair.
        """
        try:
            return blocksec2go.get_key_info(self._reader, key_id)
        except Exception as details:
            logging.error(str(details))

    def valid_key(self, key_id):
        """ Checks the specified keypair for its existence and validity.
        """
        try:
            return blocksec2go.is_key_valid(self._reader, key_id)
        except Exception as details:
            logging.error(str(details))

    def verify_pin(self, pin: str):
        """ Verifies a PIN value on the Blockchain Security 2Go card.
        """
        try:
            status = blocksec2go.verify_pin(self._reader, pin)
            if ((True == status) and (isinstance(status, bool))):
                logging.info('Card unlocked!')
                return True
            elif (0 != status):
                logging.error(str(status) + ' tries left!')
            else:
                logging.error('PIN locked!')
        except Exception as details:
            logging.error(str(details))
        return False

    def generate_signature(self, key_id, hashed_tx) -> Tuple[int, int, bytes]:
        """ Generates a signature with keypair `key_id` using `hashed_tx`.

        The returned signature is in the DER encoded format.
        No exception catching on purpose!

        Returns:
        :obj:`tuple`: (global_counter, counter, signature)
        """
        return blocksec2go.generate_signature(self._reader, int(key_id), hashed_tx)

    def get_key_id_for_public_key(self, public_key: bytes) -> int:
        for i in range(1, 255):
            (_, _, key) = self.get_keypair_info(i)
            if key == public_key:
                return i

        raise Warning("Required public key for transaction not found on card!")

    def get_valid_keys(self):
        keys = []
        for i in range(0, 255):
            try:
                (_, _, key) = self.get_keypair_info(i)
            except TypeError:
                return keys

            if not key and i != 0:
                break

            keys.append(key)
        return keys

    def encrypted_key_import(self, seed: bytes):
        blocksec2go.encrypted_keyimport(self._reader, seed)

    def wait_for_card_connected(self, timeout: float = 2000.0):
        if not self._card_connected_event.wait(timeout):
            raise TimeoutError("Card not connected within timeout.")

    def set_card_state_callback(self, card_connected_callback, card_disconnected_callback):
        self._card_connected_callback = card_connected_callback
        self._card_disconnected_callback = card_disconnected_callback
