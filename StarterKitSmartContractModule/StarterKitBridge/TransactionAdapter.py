# SPDX-FileCopyrightText: 2023 Infineon Technologies AG
# SPDX-License-Identifier: MIT

import binascii
from NFCUtils import NFCUtils
from blocksec2go_ethereum._utils import sigdecode_der, find_recovery_id
from typing import Tuple, List
from PySide2.QtCore import QObject, Signal
from CardTransaction import CardTransactionType, CardTransaction
from functools import wraps

import logging as logger


def activate_card(f):
    @wraps(f)
    def wrapper(self, *args, **kwargs):
        self._nfc_utils.wait_for_card_connected(TransactionAdapter.CARD_TIMEOUT)
        return f(self, *args, **kwargs)
    return wrapper


class TransactionAdapter(QObject):
    CARD_TIMEOUT = 3000
    card_connected: Signal = Signal(bool, object)

    def __init__(self):
        super().__init__()
        self._nfc_utils = NFCUtils()
        self._nfc_utils.set_card_state_callback(lambda i: self.card_connected.emit(True, i), lambda: self.card_connected.emit(False, None))
        self._card_info = None

    @activate_card
    def generate_signature(self, public_key: bytes, data: bytes, card_pin='') -> Tuple[int, int, int]:
        index = self._nfc_utils.get_key_id_for_public_key(public_key)

        (_, _, sig_der) = self._nfc_utils.generate_signature(index, data)
        (r, s) = sigdecode_der(sig_der)
        v = find_recovery_id((r, s), data, public_key)
        return (r, s, v)

    @activate_card
    def get_key_info(self, public_key: bytes):
        index = self._nfc_utils.get_key_id_for_public_key(public_key)
        (global_counter, counter, _) = self._nfc_utils.get_keypair_info(index)
        return {'globalSignatureCounter:': global_counter, 'signatureCounter': counter}

    @activate_card
    def get_card_info(self):
        pin_active, card_id, version = self._nfc_utils.activate_card()
        return {'pinActivation': pin_active, 'id': binascii.hexlify(card_id).decode('ascii'), "versionString": version}

    @activate_card
    def get_keys(self) -> List[bytes]:
        return self._nfc_utils.get_valid_keys()

    @activate_card
    def unlock_card(self, pin: str) -> bool:
        return self._nfc_utils.verify_pin(pin)

    @activate_card
    def generate_key(self, seed: bytes = None) -> bytes:
        if seed:
            self._nfc_utils.encrypted_key_import(seed)
            _, _, key = self._nfc_utils.get_keypair_info(0)
        else:
            key_id = self._nfc_utils.generate_keypair()
            _, _, key = self._nfc_utils.get_keypair_info(key_id)
        return key

    def handle_card_transaction(self, card_transaction: CardTransaction):
        def is_type(transaction_type: CardTransactionType):
            return card_transaction.transaction_type == transaction_type

        logger.debug(F"handle card transaction. {card_transaction.transaction_type.value}")
        if is_type(CardTransactionType.KEY_INFORMATION_REQUEST):
            return self.get_key_info(card_transaction.public_key)
        elif is_type(CardTransactionType.SIGNATURE_REQUEST):
            return self.generate_signature(card_transaction.public_key, card_transaction.data)
        elif is_type(CardTransactionType.CARD_INFORMATION_REQUEST):
            return self.get_card_info()
        elif is_type(CardTransactionType.KEY_LIST_REQUEST):
            return {'keys': [binascii.hexlify(i).decode('ascii') for i in self.get_keys()]}
        elif is_type(CardTransactionType.KEY_GENERATE_REQUEST):
            return binascii.hexlify(self.generate_key(card_transaction.seed)).decode('ascii')

    def get_readers(self) -> List[str]:
        return self._nfc_utils.get_reader_list()

    def _prepare_card(self, timeout: int = -1):
        if timeout == -1:
            timeout = TransactionAdapter.CARD_TIMEOUT

        self._nfc_utils.wait_for_card_connected(TransactionAdapter.CARD_TIMEOUT)
        self._nfc_utils.activate_card()
