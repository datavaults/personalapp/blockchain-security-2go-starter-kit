# SPDX-FileCopyrightText: 2023 Infineon Technologies AG
# SPDX-License-Identifier: MIT

from enum import Enum
from dataclasses import dataclass


class CardTransactionType(Enum):
    SIGNATURE_REQUEST = "Signature request received."
    CARD_INFORMATION_REQUEST = "Request to read card information received."
    KEY_INFORMATION_REQUEST = "Request to read key information received."
    KEY_LIST_REQUEST = "Request to read available public keys received."
    KEY_GENERATE_REQUEST = "Request to generate a new key received."
    REQUEST_TIMEOUT = "Previous request reached timeout."


@dataclass
class CardTransaction:
    transaction_type: CardTransactionType
    public_key: bytes = None
    data: bytes = None
    seed: bytes = None
