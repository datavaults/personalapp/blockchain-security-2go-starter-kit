# SPDX-FileCopyrightText: 2023 Infineon Technologies AG
# SPDX-License-Identifier: MIT

from PySide2.QtCore import QObject, Signal, Slot
from flask import Flask, request, jsonify, abort
from flask_cors import CORS, cross_origin
from werkzeug.serving import make_server
from threading import Event
from CardTransaction import CardTransaction, CardTransactionType
from functools import wraps

import logging as logger
logger.basicConfig(level=logger.DEBUG)


app = Flask(__name__)
CORS(app)


class RequestActions(QObject):
    card_transaction_request: Signal = Signal(CardTransaction)

    def start_server(self):
        self._server = make_server('127.0.0.1', 5000, app)
        self._server.serve_forever()

    def emit_signature_action(self):
        self.signature_requested.emit()

    def stop_server(self):
        self._server.shutdown()


actions = RequestActions()
main_event = Event()
request_timeout = 2.0
request_result = {}
server_busy = False


def server_idle(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        global server_busy
        if server_busy:
            abort(503)

        try:
            server_busy = True
            result = f(*args, **kwargs)
        finally:
            server_busy = False
        return result
    return wrapper


@app.route('/signature/generate', methods=['POST'])
@cross_origin()
@server_idle
def generate_signature():
    json_data = request.get_json()

    public_key = json_data.get('publicKey')
    data = json_data.get('data')
    logger.debug(f"Request signature: {public_key} - {data}")

    public_key = bytes.fromhex(public_key[2:])
    data = bytes.fromhex(data[2:])
    assert len(data) == 32

    actions.card_transaction_request.emit(CardTransaction(CardTransactionType.SIGNATURE_REQUEST, public_key=public_key, data=data))
    return _handle_result()

@app.route('/key/<string:public_key>', methods=['GET'])
@cross_origin()
@server_idle
def key_info(public_key: str):
    logger.debug(f"Key info request for key: {public_key}")
    public_key = bytes.fromhex(public_key[2:])
    actions.card_transaction_request.emit(CardTransaction(CardTransactionType.KEY_INFORMATION_REQUEST, public_key=public_key))
    return _handle_result()

@app.route('/card', methods=['GET'])
@cross_origin()
@server_idle
def card_info():
    logger.debug(f"Card info requested")
    actions.card_transaction_request.emit(CardTransaction(CardTransactionType.CARD_INFORMATION_REQUEST))
    return _handle_result()

@app.route('/card/keys', methods=['GET'])
@cross_origin()
@server_idle
def key_list():
    logger.debug(f"Card list requested")
    actions.card_transaction_request.emit(CardTransaction(CardTransactionType.KEY_LIST_REQUEST))
    return _handle_result()

@app.route('/key/generate', methods=['POST'])
@cross_origin()
@server_idle
def generate_key():
    json_data = request.get_json()
    bytes_seed = None

    if json_data:
        seed = json_data.get('seed', '')
        bytes_seed = seed.encode('utf-8')
        if len(bytes_seed) > 16:
            abort(400, "Seed must be <16 bytes!")

        bytes_seed = bytes_seed + (16 - len(bytes_seed)) * b'\0'

    logger.debug(f"Request new key: {bytes_seed}")

    actions.card_transaction_request.emit(CardTransaction(CardTransactionType.KEY_GENERATE_REQUEST, seed=bytes_seed))
    return _handle_result()


def _wait_for_event():
    main_event.clear()
    if not main_event.wait(request_timeout):
        actions.card_transaction_request.emit(CardTransaction(CardTransactionType.REQUEST_TIMEOUT))
        abort(504)


def _handle_result():
    _wait_for_event()

    if not request_result:
        abort(500)

    return jsonify(request_result)
