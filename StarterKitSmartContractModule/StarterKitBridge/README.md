# Smart contract module with IFX BC Security 2Go Starter Kit
This module works together with the DataVaults Personal App Frontend to integrate the Blockchain Security2Go Starter Kit for signing smart contract transactions.
An extended version of the Personal App Frontend from the branch 'starter_kit_integration' needs to be deployed to use the Starter Kit functionality.

## Starter Kit Bridge Application

This application is responsible for the low-level communication with the *Blockchain Security 2Go Starter Kit*. <br>
A local web server is started to listen for requests (default port 5000) to the API defined in *../StarterKitAPI*.

## Installation
PyScard (smartcard library for Python) requires *SWIG* to create the C/C++ bindings to Python. Manually install it (www.swig.org) or follow the instructions for the [installation](https://github.com/LudovicRousseau/pyscard/blob/master/INSTALL.md).

On Windows the simplest way is with the [Chocolatey](https://chocolatey.org/) package manager.<br>
After installing Chocolatey, run the command in an administrator shell:

    $ choco install swig


Other dependencies can be automatically installed in a virtual environment with *PIP* by running the installation script:

    $StarterKitBridge> install.bat

**or** with the manual setup:

    $StarterKitBridge> python -m venv .venv
    $StarterKitBridge> .venv\Scripts\activate.bat
    $(.venv) StarterKitBridge> pip install --upgrade pip
    $(.venv) StarterKitBridge> pip install -r requirements.txt


## Execution

Call the script to activate the virtual environment:

    $StarterKitBridge> run.bat

**or** perform the steps manually:

    $StarterKitBridge> .venv\Scripts\activate.bat
    $(.venv) StarterKitBridge> python main.py
