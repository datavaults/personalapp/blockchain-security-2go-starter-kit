openapi: 3.0.1
info:
  title: Blockchain Security 2Go Starter Kit API
  description: 'This describes the API to communicate with the Blockchain Security 2Go Starter Kit.'
  version: 1.0.0
servers:
- url: 'http://localhost'
tags:
- name: key
  description: Operations about keys
- name: signature
  description: Operations about signatures
- name: card
  description: Operations about card management
paths:
  /key/generate:
    post:
      tags:
      - key
      summary: Generates a new key pair on the attached card. If no seed is supplied, a persistent key entry on the card is randomly generated and stored.
      requestBody:
        description: Additional data required for key generation
        content:
          application/json:
            schema:
              type: object
              properties:
                seed:
                  type: string
                  format: hex
                  description: Optional seed for encrypted key import
      responses:
        200:
          description: New key generated
          content:
            application/json:
              schema:
                type: string
                format: hex
                description: The newly generated public key
        504:
          description: No card connected or error communicating with the NFC reader
        507:
          description: Key storage on card is full
  /key/{publicKey}:
    get:
      tags:
      - key
      summary: Read the key information from the card.
      parameters:
      - name: publicKey
        in: path
        description: Public key for reading the status information
        required: true
        schema:
          type: string
          format: hex
      responses:
        200:
          description: Information read successfully.
          content:
            application/json:
              schema:
                type: object
                properties:
                  globalSignatureCounter:
                    type: integer
                    description: Remaining signatures of the card.
                  signatureCounter:
                    type: integer
                    description: Remaining signatures for the given key.
        404:
          description: The given public key was not found on the card
        504:
          description: No card connected or error communicating with the NFC reader
  /signature/generate:
    post:
      tags:
      - signature
      summary: Generate a signature for the given key and data.
      requestBody:
        description: Additional data required for signature generation
        content:
          application/json:
            schema:
              type: object
              properties:
                publicKey:
                  type: string
                  format: hex
                  description: Public key for signature generation
                data:
                  type: string
                  format: hex
                  description: Data (32 bytes, hex) to be signed.
      responses:
        200:
          description: Information read successfully.
          content:
            application/json:
              schema:
                type: object
                properties:
                  r:
                    type: string
                    format: hex
                    description: R-value of signature
                  s:
                    type: string
                    format: hex
                    description: S-value of signature
                  v:
                    type: string
                    format: hex
                    description: V-value of signature (recovery identifier, 0 or 1)
        404:
          description: The given public key was not found on the card
        423:
          description: Too many failed authentications, card locked
        504:
          description: No card connected or error communicating with the NFC reader
          
  /card:
    get:
      tags:
      - card
      summary: Read the card information.
      responses:
        200:
          description: Information read successfully.
          content:
            application/json:
              schema:
                type: object
                properties:
                  pinActivation:
                    type: boolean
                    description: Indication whether PIN is active (true) or inactive (false)
                  id:
                    type: string
                    description: Card ID
                  versionString:
                    type: string
                    description: Information about the current version (ASCII encoded)
        504:
          description: No card connected or error communicating with the NFC reader
          
  /card/keys:
    get:
      tags:
      - card
      summary: Read the available public keys from the card.
      responses:
        200:
          description: Information read successfully.
          content:
            application/json:
              schema:
                type: object
                properties:
                  keys:
                    type: array
                    items:
                      type: string
                      format: hex
                    description: Array of strings
        504:
          description: No card connected or error communicating with the NFC reader
